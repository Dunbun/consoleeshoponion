using ConsoleEShop_Low;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace Tests
{
    public class Tests
    {
        private DBWorker _dBWorker;
        private Goods _newGoods;
        private Order _newOrder;
        private RegisteredUserInfo _newUser;
        private const string password = "Password";

        [SetUp]
        public void Setup()
        {
            _dBWorker = new DBWorker();
            _newGoods = new Goods { Name = "Coce", Category = "Drinks", Description = "Sweet water", Price = 14M };
            _newOrder = new Order { GoodsName = "Coce", Id = 1, Status = OrderStatus.New, UserId = 1 };
            _newUser = new RegisteredUserInfo { Role = UserRoles.Registered, Id = 1, Name = "User", Email = "Email", Password = "Password".GetHashCode().ToString() };
    }

        [Test]
        public void GetGoodsList_WithNoGoods_ReturnsEmptyList()
        {
            var actual = _dBWorker.GetGoodsList();

            var expected = new List<Goods>();

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetGoodsList_With1AddedGoods_ReturnsListCount1()
        {
            _dBWorker.CreateGoods(_newGoods);

            var actual = _dBWorker.GetGoodsList().Count;

            var expected = new List<Goods> { _newGoods }.Count;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetGoodsList_With1AddedGoods_ReturnsTheSameGoods()
        {
            _dBWorker.CreateGoods(_newGoods);

            var actual = _dBWorker.GetGoodsList();

            var expected = new List<Goods> { _newGoods };

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void AddNewGoods_IfArgumentNull_ThrowsArgumentNullException()
        {
            var expected = new ArgumentNullException();
            Exception actual = null;

            try
            {
                _dBWorker.CreateGoods(null);
            }
            catch(Exception ex)
            {
                actual = ex;
            }

            Assert.AreEqual(expected.GetType(), actual.GetType());
        }

        [Test]
        public void AddNewGoods_IfGoodsNameAlreadyExists_ThrowsArgumentException()
        {
            var expected = new ArgumentException();
            Exception actual = null;

            try
            {
                _dBWorker.CreateGoods(_newGoods);
                _dBWorker.CreateGoods(_newGoods);
            }
            catch (Exception ex)
            {
                actual = ex;
            }

            Assert.AreEqual(expected.GetType(), actual.GetType());
        }

        [Test]
        public void GetGoodsByName_IfArgumentIsNull_ThrowsArgumentNullException()
        {
            var expected = new ArgumentNullException();
            Exception actual = null;

            try
            {
                _dBWorker.GetGoodsByName(null);
            }
            catch (Exception ex)
            {
                actual = ex;
            }

            Assert.AreEqual(expected.GetType(), actual.GetType());
        }

        [Test]
        public void GetGoodsByName_IfNoSuchGoods_ReturnsNull()
        {
            var actual = _dBWorker.GetGoodsByName("NosuchName");

            Assert.AreEqual(null, actual);
        }

        [Test]
        public void GetGoodsByName_ReturnsCorrectValue()
        {
            _dBWorker.CreateGoods(_newGoods);
            var actual = _dBWorker.GetGoodsByName(_newGoods.Name);
            var expected = _newGoods;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void RegisterAccount_IfPasswordIsNull_ThrowsArgumentNullException()
        {
            var expected = new ArgumentNullException();
            Exception actual = null;

            try
            {
                _dBWorker.CreateAccount(_newUser.Email, null, _newUser.Name, _newUser.Role);
            }
            catch (Exception ex)
            {
                actual = ex;
            }

            Assert.AreEqual(expected.GetType(), actual.GetType());
        }

        [Test]
        public void RegisterAccount_IfEmailExists_ReturnsFalse()
        {
            var expected = false;
            
            _dBWorker.CreateAccount(_newUser.Email, "Password", _newUser.Name, _newUser.Role);
            bool actual = _dBWorker.CreateAccount(_newUser.Email, "Password", _newUser.Name, _newUser.Role);

            Assert.AreEqual(expected.GetType(), actual.GetType());
        }

        [Test]
        public void UpdateOrderStatus_ThrowsArgumentException()
        {
            var expected = new ArgumentException();
            Exception actual = null;

            try
            {
                _dBWorker.UpdateOrderStatus(-1, OrderStatus.Cancelled);
            }
            catch (Exception ex)
            {
                actual = ex; 
            }

            Assert.AreEqual(expected.GetType(), actual.GetType());
        }

        [Test]
        public void UpdateOrderStatus_ChangesOrderStatus()
        {
            _dBWorker.CreateGoods(_newGoods);
            _dBWorker.CreateAccount(_newUser.Email, password, _newUser.Name, _newUser.Role);
            _dBWorker.CreateNewOrder(_newOrder.GoodsName, _newOrder.UserId);

            var expected = OrderStatus.Cancelled;
            
            _dBWorker.UpdateOrderStatus(1, OrderStatus.Cancelled);

            var actual = _dBWorker.GetOrderStatus(1); 

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetOrderStatus_ThrowsArgumentException()
        {
            var expected = new ArgumentException();
            Exception actual = null;

            try
            {
                _dBWorker.GetOrderStatus(-1);
            }
            catch (Exception ex)
            {
                actual = ex;
            }

            Assert.AreEqual(expected.GetType(), actual.GetType());
        }

        [Test]
        public void GetOrdersHistory_WithNoHistory_ReturnsEmptyList()
        {
            var actual = _dBWorker.GetOrdersHistory();

            var expected = new List<Order>();

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetOrdersHistory_WithOnlyOneCreatedOrder_ReturnsListWithThisOrder()
        {
            _dBWorker.CreateGoods(_newGoods);
            _dBWorker.CreateAccount(_newUser.Email, password, _newUser.Name, _newUser.Role);
            _dBWorker.CreateNewOrder("Coce", 1);
            

            var actual = _dBWorker.GetOrdersHistory();

            var expected = new List<Order> { _newOrder };

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetUserInfo_ThrowsArgumentException()
        {
            var expected = new ArgumentException();
            Exception actual = null;

            try
            {
                _dBWorker.GetUserInfo(_newUser.Id);
            }
            catch (Exception ex)
            {
                actual = ex;
            }

            Assert.AreEqual(expected.GetType(), actual.GetType());
        }

        [Test]
        public void GetUserInfo_ReturnsCorrectValue()
        {
            var expected = _newUser;
            
            _dBWorker.CreateAccount(_newUser.Email, password, _newUser.Name, _newUser.Role);

            var actual = _dBWorker.GetUserInfo(_newUser.Id);

            Assert.AreEqual(expected.Id, actual.Id);
        }

        [Test]
        public void ChangeUserName_ThrowsArgumentException()
        {
            var expected = new ArgumentException();
            Exception actual = null;

            try
            {
                _dBWorker.ChangeUserName(_newUser.Id, _newUser.Name);
            }
            catch (Exception ex)
            {
                actual = ex;
            }

            Assert.AreEqual(expected.GetType(), actual.GetType());
        }

        [Test]
        public void ChangeUserName_ThrowsArgumentNullException()
        {
            var expected = new ArgumentNullException();
            Exception actual = null;

            try
            {
                _dBWorker.CreateAccount(_newUser.Email, password, _newUser.Name, _newUser.Role);
                _dBWorker.ChangeUserName(_newUser.Id, "");
            }
            catch (Exception ex)
            {
                actual = ex;
            }

            Assert.AreEqual(expected.GetType(), actual.GetType());
        }

        [Test]
        public void ChangeUserName_ChangesNameCorrectly()
        {
            var expected = "new name";

            _dBWorker.CreateAccount(_newUser.Email, password, _newUser.Name, _newUser.Role);

            _dBWorker.ChangeUserName(_newUser.Id, expected);

            var actual = _dBWorker.GetUserInfo(_newUser.Id).Name;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void ChangeUserPassword_ChangesPasswordCorrectly()
        {
            var expected = true; ;
            string newPassword = "newpassword";

            _dBWorker.CreateAccount(_newUser.Email, password, _newUser.Name, _newUser.Role);

            _dBWorker.ChangeUserPassword(_newUser.Id, password, newPassword);

            var actual = _dBWorker.GetUserInfo(_newUser.Id).CheckPassword(newPassword.GetHashCode().ToString());

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void ChangeUserPassword_IfWrongPassword_ThrowsArgumentException()
        {
            var expected = new ArgumentException();
            Exception actual = null;

            try
            {
                _dBWorker.CreateAccount(_newUser.Email, password, _newUser.Name, _newUser.Role);
                _dBWorker.ChangeUserPassword(_newUser.Id, password + ".",  "new password");
            }
            catch (Exception ex)
            {
                actual = ex;
            }

            Assert.AreEqual(expected.GetType(), actual.GetType());
        }

        [Test]
        public void ChangeUserPassword_IfPasswordIsNullOrEmpty_ThrowsArgumentNullException()
        {
            var expected = new ArgumentNullException();
            Exception actual = null;

            try
            {
                _dBWorker.CreateAccount(_newUser.Email, password, _newUser.Name, _newUser.Role);
                _dBWorker.ChangeUserPassword(_newUser.Id, "", "");
            }
            catch (Exception ex)
            {
                actual = ex;
            }

            Assert.AreEqual(expected.GetType(), actual.GetType());
        }

        [Test]
        public void ChangeUserPassword_IfWrongUserId_ThrowsArgumentException()
        {
            var expected = new ArgumentException();
            Exception actual = null;

            try
            {
                _dBWorker.CreateAccount(_newUser.Email, password, _newUser.Name, _newUser.Role);
                _dBWorker.ChangeUserPassword(-1, password, "new");
            }
            catch (Exception ex)
            {
                actual = ex;
            }

            Assert.AreEqual(expected.GetType(), actual.GetType());
        }

        [Test]
        public void ChangeUserEmail_IfWrongUserId_ThrowsArgumentException()
        {
            var expected = new ArgumentException();
            Exception actual = null;

            try
            {
                _dBWorker.CreateAccount(_newUser.Email, password, _newUser.Name, _newUser.Role);
                _dBWorker.ChangeUserEmail(-1, "new email");
            }
            catch (Exception ex)
            {
                actual = ex;
            }

            Assert.AreEqual(expected.GetType(), actual.GetType());
        }

        [Test]
        public void ChangeUserEmail_IfEmailIsNullOrEmpty_ThrowsArgumentNullException()
        {
            var expected = new ArgumentNullException();
            Exception actual = null;

            try
            {
                _dBWorker.CreateAccount(_newUser.Email, password, _newUser.Name, _newUser.Role);
                _dBWorker.ChangeUserEmail(_newUser.Id, "");
            }
            catch (Exception ex)
            {
                actual = ex;
            }

            Assert.AreEqual(expected.GetType(), actual.GetType());
        }

        [Test]
        public void ChangeUserEmail_ChangesEmailCorrectly()
        {
            string newEmail = "new email";
            var expected = newEmail;

            _dBWorker.CreateAccount(_newUser.Email, password, _newUser.Name, _newUser.Role);

            _dBWorker.ChangeUserEmail(_newUser.Id, newEmail);

            var actual = _dBWorker.GetUserInfo(_newUser.Id).Email;

            Assert.AreEqual(expected, actual);
        }
    }
}