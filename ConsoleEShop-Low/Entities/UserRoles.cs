﻿namespace ConsoleEShop_Low
{
    public enum UserRoles
    {
        Guest,
        Registered,
        Admin
    }
}
