﻿namespace ConsoleEShop_Low
{
    public enum OrderStatus
    {
        New, 
        CancelledByAdmin,
        Cancelled,
        PaymentReceived,
        Sent,
        Received,
        Done
    }
}
