﻿using System;

namespace ConsoleEShop_Low
{
    class Program
    {
        static void Main(string[] args)
        {
            var application = Application.Instance;

            string input;

            Console.WriteLine("Choose the role to proceed");

            do
            {
                Console.WriteLine("1 - registered, 2 - guest");
                input = Console.ReadLine();

                if (!int.TryParse(input, out var cmd))
                {
                    Console.WriteLine("Cannot parse command");

                    continue;
                }

                if (cmd == 1)
                {
                    Console.WriteLine("Enter credentials");

                    Console.WriteLine("Enter login");
                    var login = Console.ReadLine();

                    Console.WriteLine("Enter password");
                    var password = Console.ReadLine();

                    var logged = application.SecurityService.Login(login, password);

                    if (!logged)
                    {
                        Console.WriteLine("Invalid credentials");
                    }
                }
                else if (cmd == 2)
                {
                    Console.WriteLine("Guest choosen");
                }

                application.SecurityService.CurrentUser.OpenMenu();

            } while (!string.IsNullOrWhiteSpace(input));
        }
    }
}
